package com.example.thread_local_demo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xh
 * @Date 2022/10/21
 */
@Data
public class UserInfoTo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long userId;
    private String userKey;
}
