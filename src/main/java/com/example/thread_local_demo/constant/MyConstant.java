package com.example.thread_local_demo.constant;

/**
 * @author xh
 * @Date 2022/10/21
 */
public class MyConstant {
    public static final String TEMP_USER_COOKIE_NAME = "my_cookie";
    public static final int TEMP_USER_COOKIE_EXPIRE_TIME = 30;
}
