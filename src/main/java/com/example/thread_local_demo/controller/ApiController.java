package com.example.thread_local_demo.controller;

import com.example.thread_local_demo.entity.UserInfoTo;
import com.example.thread_local_demo.interceptor.MyInterceptor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.LinkedHashMap;

/**
 * @author xh
 * @Date 2022/10/21
 */
@RestController
public class ApiController {
    @GetMapping("/test")
    public String test() {
        UserInfoTo userInfoTo = MyInterceptor.threadLocal.get();
        System.out.println(userInfoTo);
        return userInfoTo.toString();
    }

    @GetMapping("/login/{id}")
    public String login(@PathVariable String id, HttpServletRequest request) {
        HttpSession session = request.getSession();
        LinkedHashMap<String, Long> loginUser = new LinkedHashMap<>();
        loginUser.put("id", 1L);
        session.setAttribute("loginUser", loginUser);
        return "登录成功：ID=" + id;
    }

}
